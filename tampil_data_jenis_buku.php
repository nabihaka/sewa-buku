<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        include 'config.php';
        $db = new Database();
    ?>

    <h2>Data Jenis Buku</h2>
    <table border="1">
        <tr>
            <th>No</th>
            <th>Kode Jenis Buku</th>
            <th>Nama Jenis Buku</th>
        </tr>
        <?php
            $no = 1;
            foreach($db->tampil_data_jenis_buku() as $x) {
        ?>
        <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo $x['kode_jenis_buku']; ?></td>
            <td><?php echo $x['nama_jenis_buku']; ?></td>
        </tr>
        <?php
            }
        ?>
    </table>
    <br><a href="tambah_data_jenis_buku.php?id=<?php echo $x['kode_jenis_buku']; ?>">Tambah data jenis buku</a></br>
</body>
</html>