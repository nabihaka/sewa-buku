<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        include 'config.php';
        $db = new Database();
    ?>

    <h2>Data Penerbit</h2>
    <table border="1">
        <tr>
            <th>No</th>
            <th>Kode Penerbit</th>
            <th>Nama Penerbit</th>
        </tr>
        <?php
            $no = 1;
            foreach($db->tampil_data_penerbit() as $x) {
        ?>
        <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo $x['kode_penerbit']; ?></td>
            <td><?php echo $x['nama_penerbit']; ?></td>
        </tr>
        <?php
            }
        ?>
    </table>
    <br><a href="tambah_data_penerbit.php?id=<?php echo $x['kode_penerbit']; ?>">Tambah data penerbit</a></br>
</body>
</html>