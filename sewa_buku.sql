-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 10 Des 2023 pada 16.06
-- Versi server: 10.4.28-MariaDB
-- Versi PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sewa_buku`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_buku`
--

CREATE TABLE `data_buku` (
  `id` int(11) NOT NULL,
  `kode_buku` varchar(30) NOT NULL,
  `judul_buku` varchar(100) NOT NULL,
  `kode_pengarang` varchar(30) NOT NULL,
  `kode_jenis_buku` varchar(30) NOT NULL,
  `kode_penerbit` varchar(30) NOT NULL,
  `isbn` varchar(30) NOT NULL,
  `tahun` year(4) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `data_buku`
--

INSERT INTO `data_buku` (`id`, `kode_buku`, `judul_buku`, `kode_pengarang`, `kode_jenis_buku`, `kode_penerbit`, `isbn`, `tahun`, `deskripsi`, `jumlah`) VALUES
(1, 'B19452', 'Kisah Kasih di Polines', 'PG20756', 'J4896', 'P11023', '10239938', '2021', 'Buku fiksi', 9),
(2, 'B48745', 'Sejarah Semarang', 'PG10843', 'R2391', 'P76849', '19292883', '2018', 'Buku sejarah', 20),
(3, 'B36927', 'Negeri 5 Menara', 'PG71852', 'J4896', 'P86830', '14628359', '2014', 'Buku fiksi', 18),
(4, 'B68259', 'Pemrograman Android', 'PG52760', 'T8874', 'P38792', '18045273', '2022', 'Buku pemrograman', 30);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_jenis_buku`
--

CREATE TABLE `data_jenis_buku` (
  `id` int(11) NOT NULL,
  `kode_jenis_buku` varchar(30) NOT NULL,
  `nama_jenis_buku` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `data_jenis_buku`
--

INSERT INTO `data_jenis_buku` (`id`, `kode_jenis_buku`, `nama_jenis_buku`) VALUES
(7, 'R2391', 'Sejarah'),
(8, 'T8874', 'Komputer'),
(9, 'J4896', 'Hiburan'),
(10, 'K1485', 'Kuliner');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_peminjam`
--

CREATE TABLE `data_peminjam` (
  `id` int(11) NOT NULL,
  `kode_peminjam` varchar(20) NOT NULL,
  `nama_peminjam` varchar(100) NOT NULL,
  `jenis_kelamin` enum('P','L') NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `data_peminjam`
--

INSERT INTO `data_peminjam` (`id`, `kode_peminjam`, `nama_peminjam`, `jenis_kelamin`, `tanggal_lahir`, `alamat`, `pekerjaan`, `user_id`, `foto`) VALUES
(1, 'A04', 'Aziz Alif Faturochman', 'L', '2004-04-10', 'Gombel', 'Mahasiswa', 33422104, ''),
(2, 'A12', 'Luqman Aldi Prawiratama', 'L', '2003-02-15', 'Sumurboto', 'Mahasiswa', 33422112, ''),
(4, 'A09', 'Hafiz Rahman Hakim', 'L', '2004-11-04', 'Sijunjung', 'Mahasiswa', 33422109, ''),
(5, 'A16', 'Nabiha Kailang Wirakrama', 'L', '2004-07-11', 'Jatingaleh', 'Mahasiswa', 33422116, ''),
(6, 'A22', 'Diana Puspitasari', 'P', '2003-04-19', 'Ngesrep', 'Mahasiswa', 0, ''),
(7, 'A07', 'Efrino Wahyu E. P.', 'L', '2002-07-16', 'Sragen', 'Presiden', 0, ''),
(8, 'A67', 'Aji Pangestu', 'L', '2003-12-20', 'Kendal', 'Mahasiswa', 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_penerbit`
--

CREATE TABLE `data_penerbit` (
  `id` int(11) NOT NULL,
  `kode_penerbit` varchar(30) NOT NULL,
  `nama_penerbit` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `data_penerbit`
--

INSERT INTO `data_penerbit` (`id`, `kode_penerbit`, `nama_penerbit`) VALUES
(8, 'P11023', 'Erlangga'),
(9, 'P38792', 'Lokomedia'),
(10, 'P76849', 'Gagas Media'),
(11, 'P86830', 'Penerbit Andi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_pengarang`
--

CREATE TABLE `data_pengarang` (
  `id` int(11) NOT NULL,
  `kode_pengarang` varchar(30) NOT NULL,
  `nama_pengarang` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `data_pengarang`
--

INSERT INTO `data_pengarang` (`id`, `kode_pengarang`, `nama_pengarang`) VALUES
(2, 'PG10843', 'Nabiha'),
(3, 'PG15573', 'Kailang'),
(4, 'PG20756', 'Wahyu'),
(5, 'PG52760', 'Aziz'),
(6, 'PG71852', 'Efrino');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_kelamin`
--

CREATE TABLE `jenis_kelamin` (
  `id` int(11) NOT NULL,
  `kode_jk` varchar(5) NOT NULL,
  `keterangan_jk` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `jenis_kelamin`
--

INSERT INTO `jenis_kelamin` (`id`, `kode_jk`, `keterangan_jk`) VALUES
(1, 'P', 'Perempuan'),
(2, 'L', 'Laki-laki');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id` int(11) NOT NULL,
  `kode_buku` varchar(30) NOT NULL,
  `kode_peminjam` varchar(30) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id`, `kode_buku`, `kode_peminjam`, `tanggal_pinjam`, `tanggal_kembali`, `status`) VALUES
(1, 'B19452', 'A12', '2023-12-10 00:00:00', '2023-12-17 00:00:00', 1),
(2, 'B36927', 'A04', '2023-12-10 00:00:00', '2023-12-17 00:00:00', 1),
(3, 'B68259', 'A16', '2023-12-10 00:00:00', '2023-12-17 00:00:00', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `akses_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_buku`
--
ALTER TABLE `data_buku`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode_buku` (`kode_buku`);

--
-- Indeks untuk tabel `data_jenis_buku`
--
ALTER TABLE `data_jenis_buku`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode_jenis_buku` (`kode_jenis_buku`);

--
-- Indeks untuk tabel `data_peminjam`
--
ALTER TABLE `data_peminjam`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_penerbit`
--
ALTER TABLE `data_penerbit`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode_penerbit` (`kode_penerbit`);

--
-- Indeks untuk tabel `data_pengarang`
--
ALTER TABLE `data_pengarang`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode_pengarang` (`kode_pengarang`);

--
-- Indeks untuk tabel `jenis_kelamin`
--
ALTER TABLE `jenis_kelamin`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_buku`
--
ALTER TABLE `data_buku`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `data_jenis_buku`
--
ALTER TABLE `data_jenis_buku`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `data_peminjam`
--
ALTER TABLE `data_peminjam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `data_penerbit`
--
ALTER TABLE `data_penerbit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `data_pengarang`
--
ALTER TABLE `data_pengarang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
