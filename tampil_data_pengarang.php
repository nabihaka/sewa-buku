<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        include 'config.php';
        $db = new Database();
    ?>
    
    <h2>Data Pengarang</h2>
    <table border="1">
        <tr>
            <th>No</th>
            <th>Kode Pengarang</th>
            <th>Nama Pengarang</th>
        </tr>
    <?php
        $no = 1;
        foreach($db->tampil_data_pengarang() as $x) {
    ?>
    <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo$x['kode_pengarang']; ?></td>
        <td><?php echo$x['nama_pengarang']; ?></td>
    </tr>
    <?php
        }
    ?>
    </table>
    <br><a href="tambah_data_pengarang.php?id=<?php echo $x['kode_pengarang']; ?>">Tambah data pengarang</a></br>
</body>
</html>